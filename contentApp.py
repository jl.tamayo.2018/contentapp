#!/usr/bin/python

from webApp import WebApp


class ContentApp(WebApp):

    content_dictionary = {
        'hola': "<html><body>Hola!</body></html>",
        'adios': "<html><body>Adios!</body></html>"
    }

    def process(self, parsed_request):
        recurso = parsed_request[1]

        if recurso in self.content_dictionary:
            http = "200 OK"
            body = self.content_dictionary[recurso]
        else:
            http = "404 Not Found"
            body = "<html><body>Not Found</body></html>"

        return http, body


if __name__ == "__main__":
    testContentApp = ContentApp("localhost", 1234)
